package com.evetools.ws;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MWD
 */
@RequestMapping(value="/api/miningtimer/")
@RestController
public class MiningTimer {
    
    private static final String MINING_FILE_NAME="miningtimer.txt";
    
    @RequestMapping(value="setTimer", method = RequestMethod.GET)
    public String setTimer(@RequestParam(value="reset", required = false, defaultValue="false") Boolean reset){
        try {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(MINING_FILE_NAME), "utf-8"))) {
                Date date = new Date();
                if (reset) {
                    writer.write("");
                } else {
                    writer.write(date.getTime()+"");
                }
                writer.close();
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(MiningTimer.class.getName()).log(Level.SEVERE, null, ex);
                return "{\"result\":\"KO\"}";
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MiningTimer.class.getName()).log(Level.SEVERE, null, ex);
                return "{\"result\":\"KO\"}";
            } catch (IOException ex) {
                Logger.getLogger(MiningTimer.class.getName()).log(Level.SEVERE, null, ex);
                return "{\"result\":\"KO\"}";
            }
            return "{\"result\":\"OK\"}";
        } catch (Exception e) {
            return "{\"result\":\"KO\"}";
        }
    }
    
    
    @RequestMapping(value="getLastTimer", method = RequestMethod.GET)
    public String getLastTimer(){
        try {
            Date date = getDate();

            if (date == null) {
                return "{\"result\":\"\"}";
            }
            
            return "{\"result\":\""+toISO8601(date)+"\"}";
        } catch (Exception e) {
            setTimer(Boolean.TRUE);
            return "{\"result\":\"KO\"}";
        }
    }

    
    @RequestMapping(value="isTime", method = RequestMethod.GET)
    public String isTime(){
        try {
            Date date = getDate();
            Date now = new Date();

            if (date != null &&
                    date.getTime() < now.getTime() &&
                    (date.getTime() + 5 * 1000) > now.getTime()) {
                return "{\"result\":\"TRUE\",\"timerTime\":\""+toISO8601(date)+"\",\"serverTime\":\""+toISO8601(now)+"\"}";
            }
            
            return "{\"result\":\"FALSE\",\"timerTime\":\"\",\"serverTime\":\""+toISO8601(now)+"\"}";
        } catch (Exception e) {
            setTimer(Boolean.TRUE);
            return "{\"result\":\"KO\"}";
        }
    }
    
    private Date getDate() {
        String millis = null;
        try (BufferedReader brTest = new BufferedReader(new FileReader(MINING_FILE_NAME))) {
            millis = brTest.readLine();
        } catch (IOException ex) {
            setTimer(Boolean.TRUE);
            Logger.getLogger(MiningTimer.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        if (millis == null || "".equals(millis.trim())) {
            return null;
        }

        return new Date(Long.parseLong(millis));
    }
    
    private String toISO8601(Date date) {
        if (date == null) 
            date = new Date();
        
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(tz);
        return df.format(date);
    }
}
